﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parse;

namespace ParseProxyPhone
{
    public sealed class ParseProxyPhone
    {
        public static void Initialize(string appId, string clientKey)
        {
            ParseClient.Initialize(appId, clientKey);
        }

        public static void Subscribe(string channel)
        {
            ParsePush.SubscribeAsync(channel);
        }

        public static void TrackOpened()
        {
            ParseAnalytics.TrackAppOpenedAsync();
        }
    }
}
