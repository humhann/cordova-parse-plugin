module.exports = {
    initialize: function (successCallback, errorCallback, args) {
        try {
            var res = ParseProxyPhone.ParseProxyPhone.initialize(args[0], args[1]);
            successCallback(res);
        }
        catch (e) {
            errorCallback(e);
        };
    },

    subscribe: function (successCallback, errorCallback, args) {
        try {
            var res = ParseProxyPhone.ParseProxyPhone.subscribe(args[0]);
            successCallback(res);
        }
        catch (e) {
            errorCallback(e);
        };
    },

    trackOpened: function(successCallback, errorCallback) {
        try {
            var res = ParseProxyPhone.ParseProxyPhone.trackOpened();
            successCallback(res);
        }
        catch (e) {
            errorCallback(e);
        };
    }
};

require("cordova/exec/proxy").add("Parse", module.exports);
